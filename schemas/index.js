const { makeExecutableSchema } = require("@graphql-tools/schema");
const gql = require("graphql-tag");

const { Author, authorResolvers } = require("./author.js");
const { Post, postResolvers } = require("./post.js");

const Query = gql`
    # the schema allows the following query:
    type Query {
        posts: [Post]
        author(id: Int!): Author
    }
    # this schema allows the following mutation:
    type Mutation {
        upvotePost(postId: Int!): Post
    }
`;

const schema = makeExecutableSchema({
    typeDefs: [ Query, Author, Post ],
    resolvers: [authorResolvers, postResolvers]
});

module.exports = { schema };
