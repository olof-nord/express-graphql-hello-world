const gql = require("graphql-tag");

const { authors, posts } = require("../data/exampleData");

const Author = gql`
    type Author {
        id: Int!
        firstName: String
        lastName: String
        # the list of Posts by this author
        posts: [Post]
    }
`;

const authorResolvers = {
    Query: {
        author: (_, { id }) => authors.find(author => author.id === id)
    },
    Author: {
        posts: author => posts.filter(post => post.authorId === author.id)
    }
};

module.exports = { Author, authorResolvers };
