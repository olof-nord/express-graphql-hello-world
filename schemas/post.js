const gql = require("graphql-tag");

const { authors, posts } = require("../data/exampleData");

const Post = gql`
    type Post {
        id: Int!
        title: String
        author: Author
        votes: Int
    }
`;

const postResolvers = {
    Query: {
        posts: () => posts,
    },
    Post: {
        author: post => authors.find(author => author.id === post.authorId)
    },
    Mutation: {
        upvotePost: (_, { postId }) => {
            const post = posts.find(post => post.id === postId);
            if (!post) {
                throw new Error(`Couldn't find post with id ${postId}`);
            }
            post.votes += 1;
            return post;
        }
    },
};

module.exports = { Post, postResolvers };
