# express-graphql-hello-world

Demo express.js application with a GraphQL runtime

## Build and run

To start locally Node is required. If you use nvm, simply issue `nvm use`.

Install the needed dependencies:

```bash
npm install
```

To start:

```bash
npm run dev
```

The GraphiQL API explorer is available at http://localhost:4000/graphql

## Example queries

Fetch all post titles.

```graphql
{
    posts {
        title
    }
}
```

```json
{
  "data": {
    "posts": [
      {
        "title": "Introduction to GraphQL"
      },
      {
        "title": "Welcome to Meteor"
      },
      {
        "title": "Advanced GraphQL"
      },
      {
        "title": "Launchpad is Cool"
      }
    ]
  }
}
```

Fetch some data from all posts, and add specific fields from the authors who wrote the books.

```graphql
{
    posts {
        title
        author {
            lastName
        }
    }
}
```

```json
{
  "data": {
    "posts": [
      {
        "title": "Introduction to GraphQL",
        "author": {
          "lastName": "Coleman"
        }
      },
      {
        "title": "Welcome to Meteor",
        "author": {
          "lastName": "Stubailo"
        }
      },
      {
        "title": "Advanced GraphQL",
        "author": {
          "lastName": "Stubailo"
        }
      },
      {
        "title": "Launchpad is Cool",
        "author": {
          "lastName": "Novikov"
        }
      }
    ]
  }
}
```

Fetch the current votes for all posts:

```graphql
{
  posts {
    id
    votes
  }
}
```

```json
{
  "data": {
    "posts": [
      {
        "id": 1,
        "votes": 2
      },
      {
        "id": 2,
        "votes": 3
      },
      {
        "id": 3,
        "votes": 1
      },
      {
        "id": 4,
        "votes": 7
      }
    ]
  }
}
```

Add one vote for post #3

```graphql
mutation {
  upvotePost(postId: 3) {
    id
    title,
    votes
  }
}
```

```json
{
  "data": {
    "upvotePost": {
      "id": 3,
      "title": "Advanced GraphQL",
      "votes": 2
    }
  }
}
```

Fetch data from a single author:

```graphql
{
  author(id: 2) {
    id
    lastName
  }
}
```

```json
{
  "data": {
    "author": {
      "id": 2,
      "lastName": "Stubailo"
    }
  }
}
```
