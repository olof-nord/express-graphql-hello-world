const { app } = require("./app");

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
    console.log(`Running a GraphQL API server on http://localhost:${PORT}/graphql!`);
});
